#!/bin/bash



# Update all the submodules

# git submodule update --init --recursive
# test $? -eq 0 || exit 1

# git submodule update --remote --merge
# test $? -eq 0 || exit 1

## QT Themes

mkdir -p config/qt5ct/colors && cp qt5ct/themes/* config/qt5ct/colors
test $? -eq 0 || exit 1

mkdir -p config/qt6ct/colors && cp qt5ct/themes/* config/qt6ct/colors
test $? -eq 0 || exit 1

## Apply Kitty Theme

kitty +kitten themes Catppuccin-Mocha

## Apply Hyprland theme

mkdir -p config/hypr && cp hyprland/themes/* config/hypr
test $? -eq 0 || exit 1

### Generate bmenu.sh

# TODO

### Generate Hypraper conf

# TODO

## Apply Waybar Theme

mkdir -p config/waybar && cp waybar/themes/* config/waybar
test $? -eq 0 || exit 1

## Apply mako Theme

mkdir -p config/mako && cp mako/src/mocha config/mako/config
test $? -eq 0 || exit 1


# Rofi

mkdir -p config/rofi && cp -r rofi/deathemonic/* config/rofi
test $? -eq 0 || exit 1

# mc

mkdir -p local/share/mc/skins && cp mc/catppuccin.ini local/share/mc/skins/
test $? -eq 0 || exit 1

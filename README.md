# HACCS

:warning: This is work in progress!!! :warning:


This is **H**yprland **A**rch **C**atppuccin **C**onfig **S**etup. A little helper script to implement a [Catppuccin](https://github.com/catppuccin/catppuccin) Hyperland environment on Arch Linux. This is for personal use, but feel free to use it (I hope it works!).

## How to Use

### Dependencies

Install the following dependencies.

#### From AUR

All [AUR](https://aur.archlinux.org/) dependencies are listed in `aurlist.txt`. A good [AUR helper](https://wiki.archlinux.org/title/AUR_helpers) should be the best way to install them. For example, with [`yay`](https://aur.archlinux.org/packages/yay):

```commandline
yay -S - < aurlist.txt
```


#### From Official Repos

Install all the packages in `pkglist.txt`:

```commandline
pacman -S - < pkglist.txt
```

### Generating the Configuration Files

Then, clone and run `sync.sh`. This will populate the `config` and `local` subfolders with configuration files. You will be responsible to migrate/merge this files with the ones in your `~/.config` and `~./local` folders. Note, however, that the kitty theme is applied right away. If you wish to rever to another theme use `kitty +kitten themes` to select your previous theme. You might want to backup your Kitty configuration just in case (`~/.config/kitty`).

### Notes

If you come from GNOME, remember to uninstall [`xdg-desktop-portal-gnome`](https://archlinux.org/packages/extra/x86_64/xdg-desktop-portal-gnome/). This is known to cause applications to have a slow startup on Wayland. For more information, see:

* [Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=275618).
* [swaywm SubReddit](https://www.reddit.com/r/swaywm/comments/13rz7ic/wayland_applications_very_slow_to_start/).
* [sway Issues](https://github.com/swaywm/sway/issues/5732).

## Credits

The waybar comnfiguration is based on [7KIR7's dots](https://github.com/7KIR7/dots).
